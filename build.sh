#!/bin/sh

docker stop partirculate
docker rm partirculate
docker build -t extropic/partirculate .
docker run --name partirculate -p 3000:3000 -d extropic/partirculate
docker logs partirculate
