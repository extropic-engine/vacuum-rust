FROM extropic/rust

COPY . /src

RUN cd /src; cargo build --release; cp target/release/vacuum .; rm -rf target

EXPOSE 3000

CMD ["/src/vacuum"]
