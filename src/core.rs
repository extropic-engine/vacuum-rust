
struct Config {
    mut params: HashMap,
}

fn env_or_default(env_var: &str, default: &str) -> String {
    match env::var(env_var) {
        Ok(val) => val,
        Err(err) => {
            println!("No {} environment variable set. Defaulting to '{}'.", env_var, default);
            default.to_string()
        }
    }
}

impl Config {
    fn new() -> Config {
        let result = Config {
            params: HashMap::new(),
        };
        result.params.insert("db_user", env_or_default("POSTGRES_ENV_POSTGRES_USER", "postgres"));
        result.params.insert("db_password", env_or_default("POSTGRES_ENV_POSTGRES_PASSWORD", "password"));
        result.params.insert("db_host", env_or_default("POSTGRES_PORT_5432_TCP_ADDR", "127.0.0.1"));
        result.params.insert("db_port", env_or_default("POSTGRES_PORT_5432_TCP_PORT", "5432"));
        result.params.insert("db_name", "vacuum");
        result.params.insert("server_host", env_or_default("SERVER_HOST", "127.0.0.1"));
        result.params.insert("server_port", env_or_default("SERVER_PORT", "3000"));
        result
    }

    fn get(&self, key: &str) -> String {
        match params.get(key) {
            Ok(val) => val,
            Err(err) => {
                println!("Tried to access nonexistent config parameter {}.", key);
                "".to_string()
            }
        }
    }
}
