extern crate iron;
extern crate router;
extern crate postgres;
extern crate irc;

extern crate vacuum;

use vacuum::*;

use std::env;
use std::net::*;

use iron::{Iron, Request, Response, IronResult};
use iron::status;
use router::Router;

use postgres::{Connection, SslMode};

use irc::client::prelude::IrcServer;
use irc::client::server::utils::ServerExt;
use irc::client::server::Server;

fn main() {
    let env_prefix = "VACUUM";

    let mut router = Router::new();
    router.get("/", default);
    router.post("/auth", controllers::auth::login);

    // connect to IRC
    //let server = IrcServer::new("config.json").unwrap();
    //server.identify().unwrap();
    //for message in server.iter() {
    //    println!("{}", message.unwrap().into_string());
    //}

    let conn = Connection::connect(&*format!("postgres://{}:{}@{}:{}/{}",
        config::get("db_user"),
        config::get("db_password"),
        config::get("db_host"),
        config::get("db_port"),
        config::get("db_name")
    ), &SslMode::None).unwrap();

    println!("Connected to database.");

    conn.execute("
      CREATE TABLE IF NOT EXISTS playlists (
        id SERIAL PRIMARY KEY,
        name VARCHAR NOT NULL
    )", &[]).unwrap();

    let mut pl = Playlist {
      id: 0,
      name: "Test Playlist".to_string(),
    };

    conn.execute("INSERT INTO playlists (name) VALUES ($1)", &[&pl.name]).unwrap();

    let stmt = conn.prepare("SELECT id, name FROM playlists").unwrap();
    for row in stmt.query(&[]).unwrap() {
      pl = Playlist {
        id: row.get(0),
        name: row.get(1)
      };
    }

    let mut host = format!("{}:{}", config::get("server_host"), config::get("server_port")).to_socket_addrs().unwrap();

    Iron::new(router).http(host.next().unwrap()).unwrap();

    fn default(req: &mut Request) -> IronResult<Response> {
        Ok(Response::with((status::Ok, "default")))
    }

    println!("Iron is running");
}
