use rustc_serialize::json;
use rustc_serialize::Encodable;

use iron::{Iron, Request, Response, IronResult};
use iron::status;

#[derive(RustcEncodable)]
pub struct AuthResponse {
    token: String,
}

pub fn login(req: &mut Request) -> IronResult<Response> {

    // TODO: check request parameters

    let body = AuthResponse {
        token: "12345".to_string(),
    };
    let response = Response::with((
        status::Ok,
        json::encode(&body).unwrap(),
    ));

    Ok(response)
}
